package finalearn;

public class Demo01Final {

    public static void main(String[] args) {
        int num1 = 10;
        System.out.println(num1);
        num1 = 20;
        System.out.println(num1);

        // 一旦使用final修饰局部变量，这个变量就不能进行修改
        // 一次赋值，终生不变
        final int num2 = 200;
        System.out.println(num2);
        // num2 = 200;  // 错误写法！

        // 正确写法，只要保证有唯一一次赋值即可
        final int num3;
        num3 = 30;

        // 对于基本类型来说，不可变说的是变量当中的数据不改变
        // 对于引用类型来说，不可变说的是变量当中的地址值不可改变
        Student stu1 = new Student("赵丽颖");
        System.out.println(stu1.getName()); // 赵丽颖
        System.out.println(stu1);
        stu1 = new Student("霍建华");
        System.out.println(stu1.getName()); // 霍建华
        System.out.println(stu1);

        System.out.println("===========");

        // final的引用类型，其中的地址不可改变
        final Student stu2 = new Student("高圆圆");
        System.out.println(stu2.getName());
        // 但是可以改变内容
        stu2.setName("赵又廷");
        System.out.println(stu2.getName());
    }
}
