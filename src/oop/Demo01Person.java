package oop;

public class Demo01Person {

    public static void main(String[] args) {
        Person person = new Person();
        person.show();

        person.name = "赵丽颖";
        person.setAge(18);
        person.show();
    }
}
