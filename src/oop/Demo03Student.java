package oop;

public class Demo03Student {

    public static void main(String[] args) {
        Student stu = new Student();

        stu.setName("鹿晗");
        stu.setMale(true);
        stu.setAge(20);

        System.out.println("姓名:" + stu.getName());
        System.out.println("年龄:" + stu.getAge());
        System.out.println("是不是爷们:" + stu.isMale());
    }
}
