package oop;

/**
 * 构造方法：
 * 1.构造方法必须和类名称完全一样
 * 2.构造方法不要写返回值类型，连void都不屑
 * 3.构造方法不能return一个具体的返回值
 * 4.如果没有写任何构造方法，编译器会默认赠送一个构造方法，没有参数，方法体什么也不做
 * 5.一旦写了至少一个构造方法，那么编译器将不再赠送
 *
 * 一个标准的类通常要拥有下面四个部分：
 * 1.所有的成员变量都要用private修饰
 * 2.为每一个成员变量编写一对Getter/Setter方法
 * 3.编写一个全参构造方法
 * 4.编写一个无参构造方法
 */
public class Student {

    private String name;  // 姓名
    private int age;    // 年龄
    private boolean male; // 性别

    public Student() {
    }

    public Student(String name, int age, boolean male) {
        this.name = name;
        this.age = age;
        this.male = male;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public boolean isMale() {
        return male;
    }
}
