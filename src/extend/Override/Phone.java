package extend.Override;

// 本来的老款手机
public class Phone {

    public Phone() {
        System.out.println("老款手机无参构造方法");
    }

    public Phone(int num) {
        System.out.println("老款手机有参构造方法");
    }

    public void call() {
        System.out.println("打电话");
    }

    public void send() {
        System.out.println("发短信");
    }

    public void show() {
        System.out.println("显示号码");
    }
}
