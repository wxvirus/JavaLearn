package extend.Override;

public class NewPhone extends Phone {

    public NewPhone() {
//        super(); // 在调用父类无参构造方法,默认有
        super(10); // 在调用父类重载的构造方法
        System.out.println("新款手机构造方法");
    }

    @Override
    public void show() {
        super.show(); // 把父类的show方法拿过来重复利用
        System.out.println("显示姓名");
        System.out.println("显示头像");
    }
}