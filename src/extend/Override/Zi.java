package extend.Override;

public class Zi extends Fu {

    int num = 20;

    public Zi() {
        this(123);
    }

    public Zi(int num) {
        this.num = num;
    }

    public void showNum() {
        int num = 10;
        System.out.println(num); // 局部变量
        System.out.println(this.num); // 本类中的成员变量
        System.out.println(super.num); // 父类的成员变量
    }

    @Override
    public String method() {
        return null;
    }
}
