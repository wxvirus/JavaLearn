package extend;

/**
 * 定义一个父类：员工
 */
public class Employee {

    int numFu = 10;

    int num = 100;

    public void method() {
        System.out.println("方法执行!");
    }

    public void methodFu() {
        // 使用的是本类当中的，不会向下找子类的
        System.out.println(num);
    }
}
