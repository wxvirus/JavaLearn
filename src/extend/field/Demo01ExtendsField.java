package extend.field;

public class Demo01ExtendsField {

    public static void main(String[] args) {
        Zi zi = new Zi();

        zi.method();
        zi.methodFu();
        zi.methodZi();

        // 创建的是子类对象，所以优先使用子类的方法
        zi.methodUse();
    }
}
