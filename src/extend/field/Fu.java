package extend.field;

public class Fu {

    int num = 10;

    public void methodFu() {
        System.out.println("父类方法执行！");
    }

    public void methodUse() {
        System.out.println("父类重名方法执行!");
    }
}
