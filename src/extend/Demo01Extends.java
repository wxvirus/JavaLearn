package extend;

public class Demo01Extends {

    public static void main(String[] args) {

        // 创建一个父类对象
        Employee employee = new Employee();
        System.out.println(employee.numFu);

        // 创建了一个子类对象
        Teacher teacher = new Teacher();
        // Teacher类中当然什么都没写，但是会继承来自父类的method方法
        teacher.method();
        System.out.println(teacher.numFu);
        System.out.println(teacher.numZi);
        System.out.println(teacher.num); // 优先使用子类
        // 这个方法是子类的，优先用子类的，如果没有再向上找
        teacher.methodZi(); // 200
        // 这个方法是再父类中定义的
        teacher.methodFu(); // 100

        // 创建另一个子类对象
        Assistant assistant = new Assistant();
        assistant.method();
    }
}
