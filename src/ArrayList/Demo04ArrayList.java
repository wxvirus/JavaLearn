package ArrayList;

import java.util.ArrayList;

/**
 * 如果希望向集合ArrayList中存储基本类型，必须使用基本类型对应的”包装类“
 *
 * 基本类型  包装类(引用类型，都位于java.lang下)
 * byte Byte
 * short Short
 * int Integer   【特殊】
 * long Long
 * float Float
 * double Double
 * char Character 【特殊】
 * boolean Boolean
 */
public class Demo04ArrayList {

    public static void main(String[] args) {
        ArrayList<String> listA = new ArrayList<>();

        ArrayList<Integer> listC = new ArrayList<>();
        listC.add(100);
        listC.add(101);
        listC.add(200);

        System.out.println(listC);

        int num = listC.get(0);
        System.out.println("第一号元素是:" + num);
    }
}
