package ArrayList;

import java.util.ArrayList;

/**
 * 数组的长度不可以改变
 * ArrayList集合的长度是可以随意改变的
 *
 * ArrayList有一个尖括号<E>代表泛型
 * 泛型：也就是装在集合当中的所有元素，全都是统一的什么类型
 * 注意：泛型只能是引用类型，不能是基本类型
 *
 * 注意事项：
 * 对于ArrayList集合来说，直接打印得到的不是地址值，而是内容
 * 如果内容是空，得到的是空的中括号:[]
 */
public class Demo01ArrayList {

    public static void main(String[] args) {
        // 创建了一个ArrayList集合，集合名称是list，里面装的都是String类型的字符串
        // 从JDK1.7+开始，右侧的尖括号内可以不写内容，但是<>本身是需要写的
        ArrayList<String> list = new ArrayList<>();
        System.out.println(list);

        // 向集合当中添加一些数据，需要用到add方法
        list.add("赵丽颖");
        list.add("古力娜扎");
        list.add("马儿扎哈");
        System.out.println(list);
    }
}
