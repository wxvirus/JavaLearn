package ArrayList;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.ArrayList;

/**
 * 自定义4个学生对象，添加到集合，并遍历
 *
 * 1. 自定义Student学生类，四个部分
 * 2. 创建一个集合，存储学生对象，泛型: <Student>
 * 3. 根据类，创建4个学生对象
 * 4. 讲4个学生对象添加到集合中: add
 * 5. 遍历集合:for、size、get
 */
public class Demo06ArrayListStudent {

    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();

        Student one = new Student("洪七公", 20);
        Student two = new Student("欧阳锋", 21);
        Student three = new Student("黄药师", 22);
        Student four = new Student("段智兴", 23);

        list.add(one);
        list.add(two);
        list.add(three);
        list.add(four);

        // 遍历集合
        for (int i = 0; i < list.size(); i++) {
            Student stu = list.get(i);
            System.out.println("姓名:" + stu.getName() + ", 年龄:" + stu.getAge());
        }

        ArrayList<String> listB = new ArrayList<>();
        listB.add("张三丰");
        listB.add("宋远桥");
        listB.add("张翠山");
        listB.add("张无忌");
        printArrayList(listB);
    }

    public static void printArrayList(ArrayList<String> list) {
        System.out.print("{");
        for (int i = 0; i < list.size(); i++) {
            String name = list.get(i);
            if (i == list.size() - 1) {
                System.out.print(name + "}");
            } else {
                System.out.print(name + "@");
            }
        }
    }
}
