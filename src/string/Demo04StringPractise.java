package string;

public class Demo04StringPractise {
    public static void main(String[] args) {
        int[] array = { 1, 2, 3};

        String result = formArrayToString(array);
        System.out.println(result);
    }

    public static String formArrayToString(int[] array) {
        String str = "[";
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                str += "word" + array[i] + "]";
            } else {
                str += "word" + array[i] + "#";
            }
        }
        return str;
    }
}
