package string;

import java.util.Arrays;

public class ArraysTest {

    public static void main(String[] args) {
        String str = "adwdqdqwdwqdcqwefefwcrgrvtwqdqwdqdqcdcdd";

        // 进行升序排列
        // 将字符串变成数组 toCharArray
        char[] chars = str.toCharArray();
        Arrays.sort(chars);  // 对字符数组进行升序排列

        // 需要倒序遍历
        for (int i = chars.length - 1; i >= 0 ; i--) {
            System.out.print(chars[i]);
        }
        System.out.println();
    }
}
