public class Main {

    public static void main(String[] args) {
        // 字符串常量
        System.out.println("ABC");
        System.out.println("");
        System.out.println("XYZ");

        // 整数常量
        System.out.println(30);
        System.out.println(-300);

        // 浮点数常量(小数)
        System.out.println(3.14);
        System.out.println(-2.5);

        // 字符常量
        System.out.println('A');
        System.out.println('6');
        // 两个单引号中间必须有且仅有一个字符，没用不行

        // 布尔常量
        System.out.println(true);
        System.out.println(false);

        // 空常量，不能直接用来打印输出
//        System.out.println(null);
    }
}
