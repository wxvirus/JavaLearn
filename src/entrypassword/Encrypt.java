/**
 * @description：TODO
 * @author ：xxx
 * @date ：2020/4/17 22:36
 */

package entrypassword;

import java.security.MessageDigest;

public class Encrypt {

    /**
     * Url md5加密
     * @param url
     * @return url
     */
    public static String getUrlSign(String url) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update((url).getBytes("UTF-8"));
            byte[] b = md5.digest();

            int i;
            StringBuffer buf = new StringBuffer();
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0) {
                    i += 256;
                }
                if (i < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(i));
            }

            url = buf.toString();
            System.out.println("result = " + url);
        } catch (Exception e) {
//            log.error("error", e);
        }
        return url;
    }
}
