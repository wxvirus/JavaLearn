package duotai;

public class Demo02Main {

    public static void main(String[] args) {
        // 创建一个笔记本电脑
        Computer computer = new Computer();
        computer.powerOn();

        // 准备一个鼠标，供电脑使用
//        Mouse mouse = new Mouse();
        // 首先进行向上转型
        USB usbMouse = new Mouse(); // 多态写法
        computer.useDevice(usbMouse);

        // 创建一个键盘
        Keyboard keyboard = new Keyboard(); // 没有使用多态写法
        // 方法参数是USB类型，传递进去的是实现类
        computer.useDevice(keyboard);   // 正确写法
        // 使用子类对象，匿名对象也可以
        computer.useDevice(new Keyboard());   // 正确写法
        computer.powerOff();

        System.out.println("===============");

        method(10.0); // double --> double
        method(20);   // int --> double
        int a = 30;
        method(a); // int --> double
    }

    public static void method(double num) {
        System.out.println(num);
    }
}
