package duotai;

public class Demo01Main {

    public static void main(String[] args) {
        Animal animal = new Cat();
        animal.eat();

//        animal.catchMouse(); // 错误写法！！
        Cat cat = (Cat) animal;
        cat.catchMouse(); // 还原成功

        // 本来new的时候是一只猫，现在非要当成一只狗，错误写法
//        Dog dog = (Dog) animal; // 编译不会报错，运行会出现异常
        // java.lang.ClassCastException,类转换异常
    }
}
