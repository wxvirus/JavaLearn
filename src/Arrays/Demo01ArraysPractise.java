package Arrays;

import java.util.Arrays;

public class Demo01ArraysPractise {

    public static void main(String[] args) {
        String str = "adwqdqdwqd12e123891dbwduiewqdbqi";

        // 进行升序排列：sort
        // 必须是一个数组，才能使用Arrays.sort方法
        // String -》 数组，toCharArray
        char[] chars = str.toCharArray();
        Arrays.sort(chars); // 对字符数组进行排序
        
        // 需要倒序遍历
        for (int i = chars.length - 1; i >= 0; i--) {
            System.out.print(chars[i] + "\t");
        }
    }
}
