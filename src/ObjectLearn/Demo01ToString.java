package ObjectLearn;

import java.util.Random;
import java.util.Scanner;

public class Demo01ToString {
    public static void main(String[] args) {
        /**
         * Person类默认继承了Object类，所以可以视野Object种得toString
         */
        Person person = new Person("张三", 18);
        String s = person.toString();
        System.out.println(s);

        // 直接打印对线得名字，其实就是调用对象的toString
        System.out.println(person);

        // 看一个类是否重写了toString，直接打印这个类的对象即可
        // 如果没有重写，打印的是对象的地址值

        Random r = new Random();
        System.out.println(r);

        Scanner scanner = new Scanner(System.in);
        System.out.println(scanner);

//        jiujiu();
    }

    /**
     * 九九乘法表
     */
    public static void jiujiu() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j <= i ; j++) {
                System.out.print(j + "*" + i + "=" +  j*i + "\t");
            }
            // 换行效果
            System.out.println();
        }
    }
}
