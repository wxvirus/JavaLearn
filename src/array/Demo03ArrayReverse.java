package array;

/**
 * 数组元素反转
 *
 * 就是对称位置的元素反转，表示对称位置需要两个索引
 * 用第三个变量进行交换
 * min代表最左边的索引，max代表最右边的索引
 * 当min < max时，代表反转完成，停止循环，则为条件
 * min从左往右，为++
 * max从又往左，为--
 */
public class Demo03ArrayReverse {

    public static void main(String[] args) {
        int[] array = { 10, 20, 30, 40, 50 };

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        System.out.println("===============");

        for (int min = 0, max = array.length - 1;min < max; min++, max--) {
            int temp = array[min];
            array[min] = array[max];
            array[max] = temp;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
