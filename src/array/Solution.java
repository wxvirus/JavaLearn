package array;

import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        int[] guess = {1, 4, 5, 3};
        int[] answer = {2, 3, 6, 3};
        int res = game(guess, answer);
        System.out.println(res);
    }

    /**
     * 猜数字和答案
     *
     * @param guess  array
     * @param answer array
     * @return int
     */
    private static int game(int[] guess, int[] answer) {
        int res = 0;
        if (Arrays.equals(answer, guess)) {
            return 3;
        }
        for (int i = 0; i < 3; i++) {
            if (guess[i] == answer[i]) {
                res++;
            }
        }
        return res;
    }
}
