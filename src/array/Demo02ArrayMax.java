package array;

/**
 * 以比武招亲形式来实现比较数组元素最大值
 */
public class Demo02ArrayMax {

    public static void main(String[] args) {

        // 5个选手
        int[] array = { 5, 15, 30, 20, 10000};

        int max = array[0];    // 比武擂台

        for (int i = 1; i < array.length; i++) {
            // 如果当前元素，比max更大，则换人
            if (array[i] > max) {
                max = array[i];
            }
        }

        // 谁最后最厉害，就能咋max当中留下最大值
        System.out.println("最大值:" + max);
    }
}
