package quanxianxiushifu;

public class Demo01InnerClass {

    public static void main(String[] args) {
        // 外部类的对线
        Body body = new Body();
        // 通过外部类的对线，调用外部类的方法，里面间接使用内部类Heart
        body.methodBody();

        System.out.println("=====================");

        // 按照公式写
        Body.Heart heart = new Body().new Heart();
        heart.beat();
    }
}
