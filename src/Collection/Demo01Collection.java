package Collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * java.util.Collection接口
 * 所有单列集合的父接口，任意的单列集合都可以使用Collection接口中的方法
 */

public class Demo01Collection {

    public static void main(String[] args) {
        // 创建集合对象，可以使用多态
        Collection<String> coll = new ArrayList<>();
        System.out.println(coll); // 重写了toString方法 []

        /**
         * public boolean add(E e): 把给定的对象添加到集合中
         * 返回值是个boolean值，一般都返回true，所以可以不用接收
         */
        boolean b1 = coll.add("张三");
        System.out.println("b1:" + b1); // b1:true
        System.out.println(coll);   // [张三]

        coll.add("李四");
        coll.add("王五");
        coll.add("赵六");
        coll.add("田七");
        System.out.println(coll);  // [张三, 李四, 王五, 赵六, 田七]

        /**
         * public boolean remove(E e): 把给定的对象再当前集合中删除
         * 返回值是一个Boolean值，集合中存在的元素，删除元素 ，返回true
         * 集合中不存在的元素，删除失败，返回false
         */
        boolean b2 = coll.remove("赵六");
        System.out.println("b2:" + b2);

        boolean b3 = coll.remove("赵四");
        System.out.println("b3:" + b3);

        System.out.println(coll); // [张三, 李四, 王五, 田七]

        boolean b4 = coll.contains("李四");
        System.out.println("b4:" + b4); // b4:true

        boolean b5 = coll.contains("赵四");
        System.out.println("b5:" + b5); // b5:false

        // public boolean isEmpty(): 判断当前集合是否为空
        boolean b6 = coll.isEmpty();
        System.out.println("b6:" + b6); // b6:false

        int size = coll.size();
        System.out.println(size); // size: 4

        Object[] arr = coll.toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        // 简便写法
        for (Object o : arr) {
            System.out.println(o);
        }

        // 清空集合
        coll.clear();

        System.out.println(coll);
    }
}
