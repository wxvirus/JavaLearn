package Collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * java.util.Iterator:迭代器（对集合进行遍历）
 * 它是一个接口，我们无法直接使用，需要使用Iterator接口的实现类对象
 * 获取实现类的方式比较特殊
 * Collection接口中有一个方法叫做iterator()，这个方法返回的就是迭代器的实现类对象
 * <p>
 * 迭代器的使用步骤：
 * 1. 使用集合中的方法iterator()获取迭代器的实现类对象。使用Iterator接口来接收它
 * 2. 使用Iterator接口中的hasNext判断还有没有下一个元素
 * 3. 使用Iterator接口中的方法next去除集合中的下一个元素
 */

public class Demo01Iterator {

    public static void main(String[] args) {
        // 创建一个集合对象
        Collection<String> coll = new ArrayList<>();
        // 往集合中添加元素
        coll.add("姚明");
        coll.add("科比");
        coll.add("麦迪");
        coll.add("詹姆斯");
        coll.add("艾佛森");

        // Iterator<E>也是有泛型的，迭代器的泛型跟着集合走
        // 多态  接口           实现类
        Iterator<String> it = coll.iterator();

//        boolean b = it.hasNext();
//        System.out.println(b); // true
//
//        String s = it.next();
//        System.out.println(s); // 姚明

        while (it.hasNext()) {
            String e = it.next();
            System.out.println(e);
        }

        /**
         * 或者这么写，比较简单
         */
        for (String e : coll) {
            System.out.println(e);
        }

        System.out.println("===============");

        for (Iterator<String> it2 = coll.iterator(); it2.hasNext(); ) {
            String d = it2.next();
            System.out.println(d);
        }

        /**
         * 或者这么写
         */
        for (String d : coll) {
            System.out.println(d);
        }
    }
}
