package operation;

public class Demo03Operator {

    public static void main(String[] args) {
        int num1 = 10;
        System.out.println(num1);
        ++num1; // 单独使用，前++
        System.out.println(num1); // 11
        num1++;
        System.out.println(num1); // 12

        System.out.println("=============");

        // 与打印操作混合的时候
        int num2 = 20;
        // 先++，立刻马上变成21，然后打印结果21
        System.out.println(++num2);
        System.out.println(num2);
        System.out.println("=============");

        int num3 = 30;
        // 混合使用，后++，先使用本来的30，然后再加1得到31
        System.out.println(num3++); // 30
        System.out.println(num3);  // 31
        System.out.println("=============");

        // 和赋值操作混合
        int num4 = 40;
        // 前--，变量立刻马上-1，变成39，然后将39赋值给result1
        int result1 = --num4;
        System.out.println(result1); // 39
        System.out.println(num4); // 39
        System.out.println("=============");

        int num5 = 50;
        // 后--，首先把本来的数字50赋值给result2，然后自己-1变成49
        int result2 = num5--;
        System.out.println(result2); // 50
        System.out.println(num5); // 49
        System.out.println("=============");

        int x = 10;
        int y = 20;
        // 11 + 20 = 31
        int result3 = ++x + y--;   // 开发中不会出现这么复杂的，适用练习
        System.out.println(result3); // 31
        System.out.println(x); // 11
        System.out.println(y); // 19
    }
}
