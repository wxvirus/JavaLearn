package operation;

public class Demo04Operator {

    public static void main(String[] args) {
//        System.out.println(1 == 1); // true
//        System.out.println(1 < 2); // true
//        System.out.println(3 > 4); // false
//        System.out.println(3 <= 4); // true
//        System.out.println(3 >= 4); // false
//        System.out.println(3 != 4); // true
        System.out.println(true && false); // false
        System.out.println(true && true); // true
        System.out.println(true || false); // true
        System.out.println(false || false); // false

        // true && true --> true
        System.out.println(3 < 4 && 10 > 5); // true
        System.out.println(!false); // true

        int a = 10;
        // 3 > 4 -> false && ... 右边就不会执行
        System.out.println(3 > 4 && ++a < 100); // false
        System.out.println(a); // a还是10
    }
}
