package operation;

public class Demo02Plus {

    public static void main(String[] args) {

        // 字符串类型的变量的基本使用
        String str1 = "Hello";
        String str2 = "World";
        System.out.println(str1 + "\t" + str2);

        // String + int --> String
        System.out.println(str2 + 20);

        // 优先级问题
        System.out.println(str2 + 20 + 30); // World2030
        System.out.println(str2 + (20 + 30)); // World50
    }
}
