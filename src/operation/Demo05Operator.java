package operation;

public class Demo05Operator {

    public static void main(String[] args) {

        int a = 10;
        int b = 20;

        // 最大值变量
        // 判断a>b是否成立，如果成立将a的值赋值给max；如果不成立，将b的值赋值给max
        int max = a > b ? a : b;
        System.out.println(max);
    }
}
