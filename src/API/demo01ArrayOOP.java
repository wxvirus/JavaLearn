package API;

/**
 * 定义一个数组，用来存储三个Person对象
 *
 * 数组缺点：一旦创建，程序运行期间，长度不可以改变
 */
public class demo01ArrayOOP {

    public static void main(String[] args) {
        // 首先创建一个长度为3的数组，用来存储Person的对象
        Person[] array = new Person[3];
        System.out.println(array[0]);

        Person one = new Person("迪丽热巴", 18);
        Person two = new Person("古力娜扎", 28);
        Person three = new Person("马儿扎哈", 38);

        // 讲one的地址值赋值到数组的0号元素位置
        array[0] = one;
        array[1] = two;
        array[2] = three;

        System.out.println(array[0]);   // 地址值
        System.out.println(array[1]);   // 地址值
        System.out.println(array[2]);   // 地址值

        System.out.println(array[1].getName());
    }
}
