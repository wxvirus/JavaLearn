package API;

import java.util.Scanner;

/**
 * 匿名对象：只有右边对象，没有左边的名字和赋值运算
 *
 * 注意事项：匿名对象只能使用唯一一次，下次再使用不得不创建一个新的对象
 * 使用建议：如果确定有一个对象只需要使用唯一一次，就可以使用匿名对象
 */
public class Demo02Anonymous {

    public static void main(String[] args) {
        Person one = new Person();
//        one.name = "高圆圆";
//        one.showName();
//        System.out.println("==============");
//
//        // 匿名对象
//        new Person().name = "赵又廷";
//        new Person().showName(); // 我叫:null
        System.out.println("==============");

        // 使用匿名对象来进行传参
        methodParam(new Scanner(System.in));

        Scanner sc = methodReturn();
        int num = sc.nextInt();
        System.out.println("输入的是:" + num);
    }

    public static void methodParam(Scanner sc) {
        int num = sc.nextInt();
        System.out.println("输入的是:" + num);
    }

    public static Scanner methodReturn() {
        return new Scanner(System.in);
    }
}
