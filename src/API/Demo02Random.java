package API;

import java.util.Random;

/**
 * 根据一个int n的变量的值，来获取随机数组，范围是[1, n]
 *
 * 步骤：
 * 1. 定义一个int变量n，随意赋值
 * 2. 使用Random
 */
public class Demo02Random {
    public static void main(String[] args) {
        int n = 5;
        Random r = new Random();

        for (int i = 0; i < 10; i++) {
            // 本来范围是0~n-1，整体+1之后[1, n + 1),也就是[1, n]
            int result = r.nextInt(n) + 1;
            System.out.println(result);
        }
    }
}
