package API;

import java.util.Random;
import java.util.Scanner;

/**
 * 1. 首先产生一个随机数字，并且产生了，不再发生变化。用Random.nextInt()
 * 2. 需要键盘输入，所以用到了Scanner
 * 3. 获取键盘输入得数字
 * 4. 已经得到了2个数字，判断一下
 * 如果太小了，提示太小，并且重试
 * 如果太大了，提示太大，并且重试
 * 如果猜中了，游戏结束
 * 5. 重试就是再来一次，循环次数不确定，用while(true)
 */
public class Demo02RandomGame {

    public static void main(String[] args) {
        Random r = new Random();

        // [1, 100]
        int randomNum = r.nextInt(100) + 1;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("请输入你猜测得数字:");
            int guessNum = sc.nextInt();

            if (randomNum < guessNum) {
                System.out.println("太大了，请重试");
            } else if (randomNum > guessNum) {
                System.out.println("太小了，请重试");
            } else {
                System.out.println("恭喜你猜中了");
                break; // 如果猜中了，游戏结束，不再重试
            }
        }
        System.out.println("游戏结束!");
    }
}
