package API;

/**
 * @ClassName: ApiShow
 * @Description: TODO 封装一个专门返回API格式的类
 * @Author: wxvirus
 * @Date: 2020/4/25 20:46
 */
public class ApiShow<T> {

    private T data;
    private int code;
    private String msg;

    /**
     * 若没有数据返回，默认状态码喂0，提示信息为“操作成功！”
     */
    public ApiShow() {
        this.code = 0;
        this.msg = "操作成功!";
    }

    /**
     * 若没有数据返回，可以人为的知道状态码和提示信息
     *
     * @param code string
     * @param msg  string
     */
    public ApiShow(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
