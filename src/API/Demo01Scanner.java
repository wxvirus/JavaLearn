package API;

import java.util.Scanner;

/**
 * Scanner类得功能：可以实现键盘输入数据，到程序中
 *
 * 引用类型得一般使用捕捉：
 * 1.导包
 * 只有java.lang包下得内容不需要导包，其他得包都需要import
 * 2.创建
 * 3.使用
 */
public class Demo01Scanner {
    public static void main(String[] args) {
        // System.in代表从键盘输入
        Scanner sc = new Scanner(System.in);

        // 获取键盘输入的int变量
        int num = sc.nextInt();
        System.out.println("输入的int数字是:" + num);
        // 获取键盘输入的一个字符串
        String str = sc.next();
        System.out.println("输入的字符串是:" + str);
    }
}
