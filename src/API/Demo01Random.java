package API;

import java.util.Random;

/**
 * Random类用来生成随机数字
 */
public class Demo01Random {
    public static void main(String[] args) {
        Random r = new Random();

//        int num = r.nextInt();
//        System.out.println("随机数是:" + num);

        for (int i = 0; i < 100; i++) {
            int num1 = r.nextInt(10);
            System.out.print(num1 + "\t");
        }
        System.out.println();
    }
}
