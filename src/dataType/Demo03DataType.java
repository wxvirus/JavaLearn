package dataType;

/**
 * 当数据类型不一样时，会发生数据类型转换
 *
 * 自动类型转换(隐式)
 *  1. 代码不需要特殊处理，自动完成
 *  2. 规则：数据范围从小到大
 * 强制类型转换(显式)
 */
public class Demo03DataType {

    public static void main(String[] args) {
        System.out.println(1024);   // 这是一个整数，默认就是int类型
        System.out.println(3.14);   // 这就是一个浮点数，默认就是double类型

        // 左边是long类型，右边是默认的int类型，左右不一样
        // int --> long，符合数据范围从小打到的要求
        // 这一行代码发生了自动类型转换
        long num1 = 100;
        System.out.println(num1);
    }
}
