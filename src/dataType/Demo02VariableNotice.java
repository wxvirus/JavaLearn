package dataType;

/**
 * 使用变量的注意事项
 * 1. 创建多个变量，变量之间的名称不可以重复
 * 2. 对于float和long来说，字母后缀F和L不要丢掉
 * 3. 如果使用byte和short类型的变量，右侧的数值范围不能超过左侧的数据类型范围
 * 4. 变量一定要先赋值才能使用
 * 5. 变量使用不能超过作用域的范围
 * 6. 可以通过一个语句创建多个变量，一般不推荐
 */
public class Demo02VariableNotice {

    public static void main(String[] args) {

        int num1 = 10;
//        int num1 = 20;    名称不能重复
    }
}
