package dataType;

/**
 * 创建一个变量并且使用的格式：
 * 数据类型 变量名称;
 * 变量名称 = 数据值; // 赋值
 */
public class Demo01 {

    public static void main(String[] args) {

        int num1;
        // 向变量当中存入一个数据
        num1 = 10;
        // 打印输出变量的值
        System.out.println(num1);

        int num2 = 20;
        System.out.println(num2);

        long num3 = 30000000000000000L;
        System.out.println(num3);

        float num4 = 2.5F;
        System.out.println(num4);

        double num5 = 1.2;
        System.out.println(num5);

        char zifu1 = 'A';
        System.out.println(zifu1);

        zifu1 = '中';
        System.out.println(zifu1);

        boolean var1 = true;
        System.out.println(var1);

        var1 = false;

        boolean var2 = var1;
        System.out.println(var2);

//        chengfa();
    }

    /**
     * 9*9乘法表
     */
    private static void chengfa() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j <= i;j++) {
                System.out.print(j + "*" + i + "=" + j*i + "\t");
            }
            System.out.println();
        }
    }
}
