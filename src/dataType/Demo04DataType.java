package dataType;

public class Demo04DataType {

    public static void main(String[] args) {

        // 左边是int，右边是long类型，不一样
        // long --> int,不是从小大大，不能发生自动类型转换
        // 范围小的类型，范围小的变量名= (范围小的类型)原本范围大的数据;
        int num = (int)100L;
        System.out.println(num);

        // long强制类型转换成int类型，发生数据损失
        int num2 = (int)6000000000L;
        System.out.println(num2);

        // double --> int 强制类型转换，精度损失
        int num3 = (int)3.5;
        System.out.println(num3);

        char zifu1 = 'A';
        System.out.println(zifu1 + 1); // 66,也就是大写字母A当成65处理
        // 计算机的底层会用一个数字(二进制)来代表字符A 就是65

        byte num4 = 40;
        byte num5 = 50;
        // byte + byte --> int + int --> int
        int result1 = num4 + num5;
        System.out.println(result1);

        short num6 = 60;
        // byte + short --> int + int --> int
        // int强制类型转换为short：注意必须保证逻辑上真实大小没用超过short范围，否则发生数据溢出
        short result2 = (short) (num4 + num6);
        System.out.println(result2);
    }
}
