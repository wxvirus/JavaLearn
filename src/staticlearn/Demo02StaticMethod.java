package staticlearn;

/**
 * 一旦使用了static修饰成员方法，那么就成为了静态方法，不属于对象，是属于类的
 * 无论是成员变量还是成员方法，如果有了static，都推荐使用类名称进行调用
 * 静态变量：类名称.静态变量
 * 静态方法：类名称.静态方法
 *
 * 注意事项：
 * 1. 静态不能直接访问非静态
 * 原因：因为再内存中【先】有静态内容，【后】有非静态内容
 * 2. 静态方法中不能用this
 * this代表当前对象，通过谁调用的方法，谁就是当前对象
 */
public class Demo02StaticMethod {

    public static void main(String[] args) {
        MyClass obj = new MyClass(); // 首先创建对象
        // 然后才能使用没有static的关键字的内容
        obj.method();

        // 对于静态方法来说，可以通过对象名进行调用，也可以直接通过类名称调用
        MyClass.methodStatic();

        // 对于本类当中的静态方法，可以省略类名称
        myMethod();
        Demo02StaticMethod.myMethod(); // 两者等效
    }

    public static void myMethod() {
        System.out.println("自己的静态方法。");
    }
}
