package func;

import java.util.Scanner;

/**
 * @ClassName: Calc
 * @Description: TODO
 * @author: wxvirus
 * @Date: 2020/5/15 22:21
 */
public class Calc {

    public static void main(String[] args) {
        boolean flag = true;
        while (flag) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入第一个数:");
            int firstNum = scanner.nextInt();
            System.out.println("请选择执行操作:");
            String action = scanner.next();
            System.out.println("请输入第二个数");
            int secondNum = scanner.nextInt();
            switch (action) {
                case "+":
                    System.out.println(firstNum + secondNum);
                    break;
                case "-":
                    System.out.println(firstNum - secondNum);
                    break;
                case "*":
                    System.out.println(firstNum * secondNum);
                    break;
                case "/":
                    System.out.println(firstNum / secondNum);
                    break;
                case "e":
                    System.out.println("程序执行结束");
                    scanner.close();
                    flag = false;
                    break;
            }
        }
    }
}
