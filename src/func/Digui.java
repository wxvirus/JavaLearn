package func;

/**
 * @ClassName: Digui
 * @Description: 学习递归
 * @author: wxvirus
 * @Date: 2020/5/15 20:52
 */
public class Digui {

    public static void main(String[] args) {
        System.out.println(F(10));
    }

    /**
     * 递归方法实现阶乘
     * @param n
     * @return int
     */
    public static int F(int n) {
        if (n == 1) {
            return 1;
        }
        return n * F(n - 1);
    }
}
