package demoAbstract;

public abstract class Animal {

    // 这是一个抽象方法，代表吃东西，但是具体吃什么，不确定
    public abstract void eat();

    public abstract void sleep();
}
