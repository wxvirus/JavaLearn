package Interface;

/**
 * 接口就是多个类的公共规范
 * 接口是一种引用数据类型，最重要的内容就是其中的抽象方法
 * 定义接口格式：
 *  public interface 接口名称 {
 *      // 接口内容
 *  }
 *
 *  备注：缓存了关键字interface之后，编译生成之后的字节码文件仍然是.class
 *  如果是java7，那么接口中可以包含的内容有：
 *      1. 常量
 *      2. 抽象方法
 *
 *  如果是java8，还可以包含有：
 *      3. 默认方法
 *      4. 静态方法
 *
 *  如果是java9，还可以额外包含有：
 *      5. 私有方法
 */
public interface Demo01Interface {

    // 这是一个抽象方法
    public abstract void methodAbs();

    // 这也是抽象方法
    public void methodAbs1();

    // 这也是抽象方法
    void methodAbs2();

    // 这也是抽象方法
    abstract void methodAbs3();
}
