package Interface;

public class InterfaceAbstractImpl implements Demo01Interface {
    @Override
    public void methodAbs() {
        System.out.println("这是第一个抽象方法");
    }

    @Override
    public void methodAbs1() {
        System.out.println("这是第2个抽象方法");
    }

    @Override
    public void methodAbs2() {
        System.out.println("这是第3个抽象方法");
    }

    @Override
    public void methodAbs3() {
        System.out.println("这是第4个抽象方法");
    }
}
