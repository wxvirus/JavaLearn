package Interface;

public class MainAbsInt {
    public static void main(String[] args) {

        // 创建实现类的对象使用
        InterfaceAbstractImpl impl = new InterfaceAbstractImpl();
        impl.methodAbs();
        impl.methodAbs1();
        impl.methodAbs2();
        impl.methodAbs3();
    }
}
